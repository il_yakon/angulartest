import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// import { AppModule } from './app/app.module';
import { MyApp1Module } from "./myApp1/myApp1.module";


// platformBrowserDynamic().bootstrapModule(AppModule)
platformBrowserDynamic().bootstrapModule(MyApp1Module)
  .catch(err => console.error(err));
