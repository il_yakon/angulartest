import { Component } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent {
  input1 = 'input value 1';
  input2 = 'inputValue2';
  showSecondInput = true;
}
