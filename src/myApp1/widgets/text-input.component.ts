import {Component, ElementRef, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'app-text-input',
  template: `
    <div>
      <input #inputElement1 [(ngModel)]="value" placeholder="(enter value)" />
      <input [(ngModel)]="value" placeholder="(enter value)" />
      <div>
<!--        input1: {{input1 ? '"' + input1 + '"' : <span style="font-weight: lighter;">(no value)</span>/*won't work*/}}-->
        input1: <span [style]="!input1 ? 'color: red;' : ''">{{input1 ? '"' + input1 + '"' : '(no value)'}}</span>,
        input2: "{{input2}}",
      </div>
      placeholder: "{{inputElement1Ref?.nativeElement?.placeholder}}"
      value: "{{inputElement1.value}}"
    </div>
  `,
  styles: [`
    div {
      display: flex;
      flex-direction: row;
      gap: 10px;
    }
    input {
      background: #e8e8e8;
    }
  `],
})
export class TextInputComponent {
  value = '';

  @Input() input1 = '';
  private _input2 = '';
  @ViewChild('inputElement1', { static: false })
  inputElement1Ref: ElementRef<HTMLInputElement> | undefined;

  @Input()
  public set input2(input2: string) {
    this._input2 = input2;
  }
  public get input2() {
    return this._input2;
  }
}
