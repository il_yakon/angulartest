import {Component, EventEmitter, Input, Output} from "@angular/core";


@Component({
  selector: 'app-checkbox',
  template: `<input type="checkbox" [(ngModel)]="value"/>`,
})
export class CheckboxComponent {
  _value = false;

  @Input() get value() { return this._value; }

  @Output() valueChange = new EventEmitter<boolean>();
  public set value(value: boolean) {
    this._value = value;
    this.valueChange.emit(value);
  }
}
