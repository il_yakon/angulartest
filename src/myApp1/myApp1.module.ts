import { NgModule } from "@angular/core";
import { PageComponent } from './page/page.component';
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from '@angular/forms'
import { TextInputComponent } from './widgets/text-input.component';
import {CheckboxComponent} from "./widgets/checkbox.component";

@NgModule({
  declarations: [
    PageComponent,
    TextInputComponent,
    CheckboxComponent
  ],
  imports: [ BrowserModule, FormsModule ],
  providers: [],
  bootstrap: [ PageComponent ]
})
export class MyApp1Module { }
